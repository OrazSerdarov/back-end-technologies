11.0 Basis-Erreichbarkeitstest

1) Um den Webserver zu starten, öffnen Sie die Console und geben ein:
		sudo systemctl start apache2
	Danach werden Sie nach Password gefragt:
		password 

2) Um Erreichbarkeit des Webservers zu überprüfen öffnen Sie:

		a)Webbrowser in der VM und geben Sie http://localhost/
		
		b)Außerhalb der VM und geben Sie   http://10.6.3.122/
		
		wenn Port 8080 angegeben wird, landet man beim Tomcat
		
3) Um den Webserver zu stoppen öffnen Sie die Console in der VM und geben ein:

		sudo systemctl stop apache2

4) Um den Webserver zu neu zu starten, öffnen Sie die Console und geben ein:
		sudo systemctl start apache2
	Danach werden Sie nach Password gefragt:
		password 

---------------------------------

11.1 Basiskonfiguration

1) In der VM im Verzeichnis html (webapps ist Document-Root)
		/var/www/html
	mit dem Befehl: sudo touch test.html 
	leere Datei erstellen dann öffnen und Text eingeben
	
	Nun ist die Datei unter 10.6.3.122:80/test.html erreichbar
	
2) /etc/apache2/ports.conf öffnen und Listen 80 durch Listen 9876 ersetzen

			jetzt ist test.html nur über Port 9876 erreichbar 
	Alle andere Webanwendungen sind unter 8080 erreichbar

3) localhost / 10.6.3.122

4) in der Datei ports.conf 
		Listen 10.6.3.122:80 
	
5) jetzt ist nur über IP erreichbar






ports.conf ist im Verzeichnis /etc/apache2/ zu plazieren

------------------------------------------

11.2 Webdokumente publizieren

1)In /var/www/html swtpra mit sudo mkdir swtpra

2)und mit sudo touch index.html eine leere Datei

3)mit sudo touch zwei leere Dateien erstellen

4) mit sudo mkdir leeres Verzeichnis erstellen

5) mit sudo touch zwei leere Dateien im Verzeichnis a112-v1 erstellen

6) html-datei mit sudo touch erstellen und links einbauen

------------------------------

11.3 Verzeichnis mappen

1) In /home/vmuser/ mit sudo mkdir Verzeinis swtex01 erstellen
	IN swtex01 mit sudo mkdir Verzeichnis swtextra erstellen
	
2) In swtextra mit sudo touch index.html erstellen und liks einbauen

3)IN /var/www/html/swtpra/  mit sudo touch a112-d4.html leeres Datei erstellen 
	Links einbauen
	 
4)  die /etc/apache2/apache2.conf öffnen und 

	Alias /swt2/ "/home/vmuser/swtex01"
	
<Directory "/home/vmuser/swtex01/" >
		Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
</Directory>

	einfügen 
	
Jetzt sind die dateien bzw Ordner, die unter /home/vmuser/swtex01/ liegen 
	über http://localhost/swt2/ oder http://10.6.3.122:80/swt2/ erreichbar
	
	
11.4 noch Verzeichnis mappen

1,2)Wie bis jetzt sudo mkdir verzeichnis  und html mit sudo touch erstellen

3) http://<webserver>/swt2/ ist die in /home/vmuser/swtex01/ abgelegene Dateien erreichbar

























