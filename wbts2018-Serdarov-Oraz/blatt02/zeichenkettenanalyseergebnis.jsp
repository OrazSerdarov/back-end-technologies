<%-- 
    Document   : zeichenkettenanalyseergebnis
    Created on : 09.06.2018, 15:35:19
    Author     : vmuser
--%>

<!-- Oraz Serdarov -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ergebnisseite</title>
    </head>
    <body>
        <%!  
            public int countChar(String s , char c){
                
                s= s.toUpperCase();
                int j=0;
                for(int i=0 ;i<s.length();i++){
                         if(s.charAt(i)==c)
                            j++;    
                    }
                            return j;


            }
        %>
        <h1>Hier sehen Sie das Analyseergebnis zur eingegebenen Zeichenkette:</h1>
        <p>Die Zeichenkette lautet: <%= request.getParameter("paramText") %></p>
        <p>Die Zeichenkette ist: <%= request.getParameter("paramText").length() + " Zeichen lang." %></p>
        <p>Die Zeichenkette enth&auml;lt: <%= countChar(request.getParameter("paramText"),'A')+"-mal das Zeichen A oder a." %></p>
        <p>Die Zeichenkette enth&auml;lt: <%= countChar(request.getParameter("paramText"),'B')+"-mal das Zeichen B oder b." %></p>
    </body>
</html>
